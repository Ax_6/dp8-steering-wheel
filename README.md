# DP8 Steering wheel #

Following, all the documentation relative to this software 

## Summary ##

This software manages all the actions that the steering wheel has to perform, in particular, it handles

* GLCD Display
* Buttons inputs (Gears and Controls)
* Clutch input (Analog paddle)
* CAN Data communication
* Visualization of incoming CAN Data