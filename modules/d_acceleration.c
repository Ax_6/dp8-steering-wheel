//
// Created by Aaron Russo on 13/08/16.
//

#include "d_acceleration.h"

void dAcc_init(void) {
    dd_Indicator_setIntValue(ACC_TIME, dAcc_rampTime);
}

void dAcc_increaseRampTime(void) {
    dAcc_rampTime += DAAC_RAMP_TIME_STEP;
    dd_Indicator_setIntValue(ACC_TIME, dAcc_rampTime);
}

void dAcc_decreaseRampTime(void) {
    dAcc_rampTime -= DAAC_RAMP_TIME_STEP;
    dd_Indicator_setIntValue(ACC_TIME, dAcc_rampTime);
}

void dAcc_startAutoAcceleration(void) {
    dd_Dashboard_fireTimedMessage(2, "ACC ON", MESSAGE_TYPE_MESSAGE);
    dAcc_autoAcceleration = TRUE;
    dAcc_releasingClutch = FALSE;
    dAcc_setClutch(100);
    Can_writeInt(SW_RIO_GEAR_BRK_STEER_ID, RPM_LIMITER_ON);
}

void dAcc_startClutchRelease(void) {
    dAcc_releasingClutch = TRUE;
    dAcc_setRamp(DACC_RAMP_START, DACC_RAMP_END, dAcc_rampTime);
}

void dAcc_stopAutoAcceleration(void) {
    dAcc_autoAcceleration = FALSE;
}

char dAcc_isAutoAccelerationActive(void) {
    return dAcc_autoAcceleration;
}

char dAcc_isReleasingClutch(void) {
    return dAcc_releasingClutch;
}

void dAcc_setRamp(unsigned char start, unsigned char end, unsigned int time) {
    dAcc_steps = (unsigned int) ((time / 10.0) + 0.5);
    dAcc_rampStep = (double) (start - end) / (double) dAcc_steps;
    dAcc_endingRampValue = end;
    dAcc_ramping = TRUE;
}

void dAcc_setClutch(unsigned char clutchValue) {
    unsigned char paddleValue;
    paddleValue = dPaddle_getValue();
    if (paddleValue > dAcc_currentClutchValue) {
        dClutch_set(paddleValue);
        dAcc_currentClutchValue = paddleValue;
    } else {
        dClutch_set(clutchValue);
        dAcc_currentClutchValue = clutchValue;
    }
}

void dAcc_tick(void) {
    if (dAcc_ramping) {
        dAcc_setClutch(dAcc_endingRampValue + (dAcc_rampStep * dAcc_steps));
        if (dAcc_steps > 0) {
            dAcc_steps -= 1;
        } else {
            dAcc_ramping = FALSE;
            dAcc_stopAutoAcceleration();
            Can_writeInt(SW_RIO_GEAR_BRK_STEER_ID, RPM_LIMITER_OFF);
        }
    } else {
        dAcc_setClutch(dAcc_currentClutchValue);
    }
}