//
// Created by Aaron Russo on 13/07/16.
//

#include "d_ebb.h"

int dEbb_localValue = 0, dEbb_value = 0, dEbb_motorState = 0;
unsigned int dEbb_motorSense = 0;
int dEbb_calibration = EBB_CENTER_CALIBRATION;
unsigned char dEbb_isCalibrating = FALSE;

void dEbb_init(void) {
    dEbb_calibrate(EBB_CENTER_CALIBRATION);
}

void dEbb_calibrate(unsigned char calibrationValue) {
    Can_writeByte(SW_EBB_ID, calibrationValue);
}

void dEbb_increase(void) {
    if (!dEbb_isCalibrating) {
        if (dEbb_localValue < MAX_EBB_VALUE) {
            dEbb_localValue += 1;
            dEbb_propagateSteeringWheelChange(EBB_INCREASED_ACTION);
        }
    }
}

void dEbb_decrease(void) {
    if (!dEbb_isCalibrating) {
        if (dEbb_localValue > MIN_EBB_VALUE) {
            dEbb_localValue -= 1;
            dEbb_propagateSteeringWheelChange(EBB_DECREASED_ACTION);
        }
    }
}

void dEbb_setEbbValueFromCAN(unsigned int value) {
    if (value == EBB_IS_CALIBRATING) {
        dEbb_isCalibrating = TRUE;
    } else {
        dEbb_isCalibrating = FALSE;
        dEbb_value = value - EBB_DAGO_OFFSET;
    }
    dEbb_propagateEbbChange();
}

void dEbb_setEbbMotorStateFromCAN(unsigned int motorState) {
    dEbb_motorState = motorState;
}

void dEbb_setEbbMotorSenseFromCAN(unsigned int motorSense) {
    dEbb_motorSense = motorSense;
}

void dEbb_propagateSteeringWheelChange(unsigned char action) {
    char textMessage[2];
    if (action == EBB_INCREASED_ACTION) {
        textMessage[0] = '>';
    } else {
        textMessage[0] = '<';
    }
    dd_Indicator_setStringValue(EBB, textMessage);
    Can_writeByte(SW_EBB_ID, (unsigned char) (dEbb_localValue + EBB_DAGO_OFFSET));
}

void dEbb_propagateEbbChange(void) {
    if (dEbb_isCalibrating) {
        dd_Indicator_setStringValue(EBB, "Cal");
    } else {
        dd_Indicator_setIntValue(EBB, dEbb_value);
    }
}