//
// Created by Aaron Russo on 13/07/16.
//

#ifndef DP8_DISPLAY_CONTROLLER_D_EBB_H
#define DP8_DISPLAY_CONTROLLER_D_EBB_H

#include "display/dd_dashboard.h"
#include "d_can.h"

//Devo scrivergli ebb target 10Hz
//cosa è ebb target. (unisgned int? ahah) sarà signed.
//tra 0 e 8 indica bilancio
//se eeb target vale 15, il signorino deve calibrare
//calibrazione tra 10 e 20

#define EBB_CENTER_CALIBRATION 15
#define EBB_MAX_VALUE 4
#define EBB_MIN_VALUE -4
#define EBB_DAGO_OFFSET 4
#define EBB_IS_CALIBRATING 100
#define EBB_MOTOR_STATE_READY 0
#define EBB_MOTOR_STATE_BUSY 1
#define EBB_MOTOR_STATE_BROKEN 2
#define EBB_MOTOR_STATE_CALIBRATION_DONE 3
#define EBB_INCREASED_ACTION 1
#define EBB_DECREASED_ACTION 0

void dEbb_init(void);

void dEbb_calibrate(unsigned char calibrationValue);

void dEbb_increase(void);

void dEbb_decrease(void);

void dEbb_setEbbValueFromCAN(unsigned int value);

void dEbb_setEbbMotorStateFromCAN(unsigned int motorState);

void dEbb_setEbbMotorSenseFromCAN(unsigned int motorSense);

void dEbb_propagateEbbChange(void);

void dEbb_propagateSteeringWheelChange(unsigned char action);

#endif //DP8_DISPLAY_CONTROLLER_D_EBB_H
